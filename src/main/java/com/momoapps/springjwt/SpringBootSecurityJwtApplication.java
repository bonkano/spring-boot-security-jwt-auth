package com.momoapps.springjwt;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.momoapps.springjwt.models.ERole;
import com.momoapps.springjwt.models.Role;
import com.momoapps.springjwt.repository.RoleRepository;

@SpringBootApplication
public class SpringBootSecurityJwtApplication implements CommandLineRunner {
	@Autowired
	private RoleRepository roleRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSecurityJwtApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// TODO Auto-generated method stub 
		/*
		 * Role roleUser = new Role(ERole.ROLE_USER); Role roleAdmin = new
		 * Role(ERole.ROLE_ADMIN); Role roleMandator = new Role(ERole.ROLE_MODERATOR);
		 * 
		 * this.roleRepository.save(roleUser); this.roleRepository.save(roleAdmin);
		 * this.roleRepository.save(roleMandator);
		 */

	}

}
